/// <reference path="./models/dragDropInterfaces.ts"/>
/// <reference path="./models/projectModel.ts"/>
/// <reference path="./store/projectState.ts"/>
/// <reference path="./utils/validation.ts"/>
/// <reference path="./decorators/decorators.ts"/>
/// <reference path="./components/ProjectInput.ts"/>
/// <reference path="./components/ProjectList.ts"/>

namespace App {

    new ProjectInput();
    new ProjectList('active');
    new ProjectList('finished');

}


