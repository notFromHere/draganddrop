namespace App {
    export function AutoBind(
        _: any,
        _2: string,
        descriptor: PropertyDescriptor) {
        const baseMethod = descriptor.value;
        const adjDescriptor: PropertyDescriptor = {
            configurable: true,
            get() {
                return baseMethod.bind(this);
            }
        }
        return adjDescriptor;
    }
}
